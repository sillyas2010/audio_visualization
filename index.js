const init = () => {
    let context, analyser, src, soundArray,
        audio = document.getElementById('audio'),
        fileinput = document.getElementById('file-1'),
        partsWrapper = document.getElementById('parts-wrapper'),
        maxSoundElement = 512,
        partsCount = 32,
        partsArray = [];

    function createParts() {
        partsWrapper.innerHTML = '';
        partsArray = [];

        for (let i = 0; i <= partsCount; i++) {
            partsArray[i] = document.createElement('div');
            partsArray[i].setAttribute('id', 'part-' + i);
            partsArray[i].classList.add('part');

            partsWrapper.appendChild(partsArray[i]);
        }
    }

    function animationFrame() {
        if (!audio.paused) {
            window.requestAnimationFrame(animationFrame);
        }
        soundArray = new Uint8Array(analyser.frequencyBinCount);
        analyser.getByteFrequencyData(soundArray);

        partsArray.forEach((el, ind) => {
            soundIndex = parseInt(maxSoundElement * ind / (partsArray.length));

            if (soundIndex >= soundArray.length) {
                soundIndex = soundArray.length - 1;
            }

            el.style.height = '' + parseInt(soundArray[soundIndex] / 255 * 100) + '%';
        })
    }

    function preparation() {
        context = new AudioContext();
        analyser = context.createAnalyser();
        src = context.createMediaElementSource(audio);
        src.connect(analyser);
        analyser.connect(context.destination);

        animationFrame();
    }

    function onPlayPause() {
        if (!context) {
            preparation();
        }

        if (!audio.paused) {
            animationFrame();
        }
    };

    fileinput.onchange = (evt) => {
        let files = evt.target.files, // FileList object
            file = files[0];

        if (file && file.type.match('audio.*')) {
            let reader = new FileReader();
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);
            reader.onload = function(evt) {
                if (evt.target.readyState == FileReader.DONE) {
                    audio.src = evt.target.result;
                    createParts(partsCount, partsArray);
                }
            }
        } else {
            alert('You haven\' provided correct file');
        }
    };

    audio.onpause = onPlayPause;
    audio.onplay = onPlayPause;
};

$(document).ready(init);